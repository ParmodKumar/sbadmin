import React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


 

export default function Additionals() {

  return (
      <React.Fragment>
        <div>
        <Accordion>
            <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
            >
            <Typography className='heading'>Accordion 1</Typography>
            </AccordionSummary>
            <AccordionDetails>
            <Typography>
                Lorem
            </Typography>
            </AccordionDetails>
        </Accordion>
        </div>
    </React.Fragment>
  );
}
