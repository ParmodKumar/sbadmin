import React from 'react';
import {Switch } from 'react-router-dom';
import Sidebarnav from './components/sidebar-nav/Sidebarnav'
const App = () => {
  return (
      <React.Fragment>
          <Switch>
            <Sidebarnav />
          </Switch>
      </React.Fragment>
  );
}

export default App;
